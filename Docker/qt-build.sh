#!/bin/bash

qmake CONFIG+=release .
make
cp release/*.exe /output/
cp release/*.dll /output/
python2 /opt/ldd/ldd-win.py /output/
cp -R /usr/lib/mxe/usr/i686-w64-mingw32.shared/qt5/plugins/* /output/
