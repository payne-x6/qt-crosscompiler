#!/usr/bin/python
#
# ldd-win.py
#
# A similar Linux ldd command for EXE files...
#
# When invoking LoadLibrary or CreateProcess or ShellExecute on Windows environments
# a fully qualified path must be specified. Otherwise the search path goes as follows:
#
# 1) directory from which the application loaded
# 2) system directory
# 3) 16-bit system directory
# 4) Windows directory
# 5) current directory
# 6) directories listed in the PATH
#
# (c) juspayne69
#

import dll_blacklist as DLL
import pefile
import sys
from shutil import copyfile
import os
import fnmatch

def locate(pattern, root=os.curdir):
    '''Locate all files matching supplied filename pattern in and below
    supplied root directory.'''
    for path, dirs, files in os.walk(os.path.abspath(root)):
        for filename in fnmatch.filter(files, pattern):
            yield os.path.join(path, filename)


directory=sys.argv[1]
visited, queue = set(), set()

if len(sys.argv) != 2:
  print "usage: %s <directory>" %sys.argv[0]
  sys.exit(1)

for root, dirs, files in os.walk(directory):
  for filename in files:
    queue.add(filename)

while queue:
  vertex = queue.pop()
  if (vertex not in DLL.blacklist) and (vertex not in visited):
    for abs_path in locate(vertex, "/usr/lib/mxe/usr/i686-w64-mingw32.shared/"):
      copyfile(abs_path, "/output/"+vertex)
      print abs_path
      break
    visited.add(vertex)
    try:
      pe = pefile.PE(directory + vertex)
      pe.parse_data_directories()
      queue |= set(map((lambda x: x.dll), pe.DIRECTORY_ENTRY_IMPORT))
    except OSError:
      pass

sys.exit(0)

